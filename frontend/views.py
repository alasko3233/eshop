from django.shortcuts import render

# Create your views here.


def home(request):
    return render(request, "pages/index.html")


def shop(request):
    return render(request, "pages/shop.html")


def cart(request):
    return render(request, "pages/cart.html")


def contact(request):
    return render(request, "pages/contact.html")


def detail(request):
    return render(request, "pages/detail.html")


def checkout(request):
    return render(request, "pages/checkout.html")
